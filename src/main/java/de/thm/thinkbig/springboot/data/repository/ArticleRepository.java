package de.thm.thinkbig.springboot.data.repository;

import de.thm.thinkbig.springboot.data.entity.Article;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleRepository extends PagingAndSortingRepository<Article, Long> {
}
