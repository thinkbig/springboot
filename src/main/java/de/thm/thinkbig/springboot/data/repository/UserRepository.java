package de.thm.thinkbig.springboot.data.repository;

import de.thm.thinkbig.springboot.data.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    List<User> findAll();

    Optional<User> findById(Long userId);

    User save(User user);

    void deleteById(Long userId);
}
