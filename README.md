# Think Big Workshop - Spring Boot

Hier sind alle Workshop-Inhalte, die an dem Tag stattgefunden haben.

![photo5379743071828291294.jpg](photo5379743071828291294.jpg)
![photo5379766934666587189.jpg](photo5379766934666587189.jpg)
![photo5379766934666587190.jpg](photo5379766934666587190.jpg)
![photo5382304336920554329.jpg](photo5382304336920554329.jpg)

Vielen herzlchen Dank an alle Helfer und Teilnehmer!
Der Workshop hat wirklich viel Spaß gemacht.