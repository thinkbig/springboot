package de.thm.thinkbig.springboot.service;

import de.thm.thinkbig.springboot.data.entity.Article;
import de.thm.thinkbig.springboot.data.entity.User;
import de.thm.thinkbig.springboot.data.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleService {

    private final ArticleRepository articleRepository;

    @Autowired
    public ArticleService(final ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    public Page<Article> getAll(Pageable pageable) {
        return articleRepository.findAll(pageable);
    }

    public Article getById(Long articleId) {
        return articleRepository.findById(articleId).get();
    }

    public Article save(Article article) {
        return articleRepository.save(article);
    }

    public void delete(Long userId) {
        articleRepository.deleteById(userId);
    }
}
