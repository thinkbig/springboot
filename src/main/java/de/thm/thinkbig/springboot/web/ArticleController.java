package de.thm.thinkbig.springboot.web;

import de.thm.thinkbig.springboot.data.entity.Article;
import de.thm.thinkbig.springboot.data.entity.User;
import de.thm.thinkbig.springboot.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;

@RestController
@RequestMapping("/articles")
public class ArticleController {

    private final ArticleService articleService;

    @Autowired
    public ArticleController(final ArticleService articleService) {
        this.articleService = articleService;
    }

    @GetMapping
    public Page<Article> all(Pageable pageable) {
        return articleService.getAll(pageable);
    }

    @RolesAllowed("USER")
    @GetMapping("/{articleId}")
    public Article getById(@PathVariable Long articleId) {
        return articleService.getById(articleId);
    }

    @RolesAllowed("ADMIN")
    @PostMapping
    public Article create(@RequestBody Article article) {
        return articleService.save(article);
    }

    @RolesAllowed("ADMIN")
    @PutMapping
    public Article update(@RequestBody Article article) {
        return articleService.save(article);
    }

    @RolesAllowed("ADMIN")
    @DeleteMapping("/{articleId}")
    public void delete(@PathVariable Long articleId) {
        articleService.delete(articleId);
    }
}
