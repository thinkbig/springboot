INSERT INTO user (id, username)
VALUES (1, 'chefkoch_yuki'),
       (2, 'programmiererin_maki');

INSERT INTO article (id, user_id, title, text)
VALUES (1, 1, 'Erster Blogeintrag', 'Hallo, das ist ein toller Artikel über essen. :)'),
       (2, 1, 'Diesmal wieder lecker Essen', 'Wer hätte es gedacht, aber ich mache wieder ein Blog über essen. :3'),
       (3, 2, 'Ich schreibe auch', 'Hallöchen, ich bin neu hier, würde aber gerne über Spring Boot schreiben. :P');