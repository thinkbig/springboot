CREATE SEQUENCE hibernate_sequence;

CREATE TABLE user
(
    id          INTEGER         AUTO_INCREMENT PRIMARY KEY,
    username    VARCHAR(255)    NOT NULL
);

CREATE TABLE article
(
    id      INTEGER AUTO_INCREMENT PRIMARY KEY,
    user_id INTEGER,
    title   VARCHAR(255) NOT NULL,
    text    TEXT         NOT NULL,
    FOREIGN KEY (user_id) REFERENCES user (id)
);