package de.thm.thinkbig.springboot.service;

import de.thm.thinkbig.springboot.data.entity.User;
import de.thm.thinkbig.springboot.data.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository repository;

    public List<User> getAll() {
        return repository.findAll();
    }

    public User getById(Long userId) {
        return repository.findById(userId).get();
    }

    public User save(User user) {
        return repository.save(user);
    }

    public void delete(Long userId) {
        repository.deleteById(userId);
    }
}
